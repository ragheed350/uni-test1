import { Button, Col, Form, Input, Modal, Row, Space, Typography } from 'antd';
import React, { FC, useEffect, useState } from 'react';

import { useDispatch } from 'react-redux';
import { Todo } from '../../core/models/todo';
import { DeleteTodoAsync, UpdateTodoAsync } from '../../core/redux/todo';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';

import './style.css';

const { Title, Text } = Typography;

interface props {
  todo: Todo;
}

const TodoCard: FC<props> = ({ todo }) => {
  const dispatch = useDispatch();
  const [showModal, setshowModal] = useState(false);

  const delete_todo = () => {
    dispatch(DeleteTodoAsync({ id: todo.id }));
  };

  const onFinish_updateTodo = (values: Todo) => {
    dispatch(UpdateTodoAsync(values));
    setshowModal(false);
  };

  const update_modal = (
    <>
      <Modal
        title='update todo'
        visible={showModal}
        onCancel={() => setshowModal(false)}
        footer={false}
      >
        <Form name='todo' onFinish={onFinish_updateTodo}>
          <Form.Item
            label='Name'
            name='name'
            rules={[{ required: true, message: 'Please input title!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label='Discreption'
            name='discreption'
            rules={[{ required: true, message: 'Please input discreption!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type='primary' htmlType='submit'>
              Add
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );

  useEffect(() => {
    console.log(todo);
  }, [todo]);

  return (
    <>
      <Col span={24} style={{ minHeight: 200 }} className='todo-card'>
        <Row>
          <Col span={20}>
            <Title level={4}>{todo.name}</Title>
          </Col>
          <Col span={4}>
            <Space size='large'>
              <DeleteOutlined
                onClick={delete_todo}
                style={{ fontSize: '1.4em' }}
              />
              <EditOutlined
                onClick={() => setshowModal(true)}
                style={{ fontSize: '1.4em' }}
              />
            </Space>
          </Col>
          <Col span={24}>
            <Text>{todo.discreption}sss</Text>
          </Col>
        </Row>
      </Col>
      {update_modal}
    </>
  );
};
export default TodoCard;
