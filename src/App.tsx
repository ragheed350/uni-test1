import React, { useEffect } from 'react';
import {
  Switch,
  Route,
  Redirect,
  BrowserRouter as Router,
} from 'react-router-dom';
import Home from './pages';
import Login from './pages/login';
import 'antd/dist/antd.css';
import SingleTodo from './pages/single-todo';
import { selectUser } from './core/redux/app';
import { useSelector } from 'react-redux';
import Profile from './pages/profile';

const App: React.FC = () => {
  const { status } = useSelector(selectUser);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const ref_token = localStorage.getItem('ref_token');
  }, []);

  return (
    <Router basename={process.env.REACT_APP_PATH}>
      <Switch>
        <Route path='/' exact>
          <Home />
        </Route>
        <Route path='/login' component={Login} />
        <Route path='/profile'>
          {} <Profile />
        </Route>
      </Switch>
    </Router>
  );
};
export default App;
