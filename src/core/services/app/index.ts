import { Login_Req, Login_Res, User } from '../../models/user';
import ApiService from '../../../utils/api/api-service';
import ApiResult from '../../../utils/api/models/api-result';

class AppServices extends ApiService {
  constructor() {
    super({ baseURL: `https://new-backend.writables.ae/api/` });
  }

  public login = async (req: Login_Req): Promise<ApiResult<Login_Res>> =>
    this.post<Login_Res>(`public/auth/signIn`, req);

  public refreshToken = async (token?: string): Promise<ApiResult<Login_Res>> =>
    this.post<Login_Res>('public/auth/refresh-token', { refreshToken: token });

  public getUser = async (token?: string): Promise<ApiResult<User>> =>
    this.get<User>('private/user', {
      headers: { Authorization: `bearer ${token}` },
    });
}

export const appServices = new AppServices();
