import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, RootState } from '../store';
import { User, Login_Req, Login_Res } from '../../models/user';
import requestStatus from '../../../constants/enums/request-status';
import { appServices } from '../../services/app';
import isError from '../../../utils/helpers/is-error';
import { notification } from 'antd';

interface UsersState {
  status: requestStatus;
  user?: User;
  auth?: Boolean;
}

let initialState: UsersState = {
  status: 'no-thing',
};

const AppSlice = createSlice({
  name: 'Users',
  initialState,
  reducers: {
    setStatus: (state, { payload }: PayloadAction<requestStatus>) => {
      state.status = payload;
    },
    setUser: (state, { payload }: PayloadAction<User>) => {
      state.user = payload;
    },
    setAuth: (state, { payload }: PayloadAction<Boolean>) => {
      state.auth = payload;
    },
  },
});

const { setStatus, setUser, setAuth } = AppSlice.actions;

export const LoginAsync = (req: Login_Req): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));
  const result = (await appServices.login(req)) as any;

  if (isError(result)) {
    const data: any = result;
    dispatch(setStatus('error'));

    notification.error({ message: data?.errors[0].msg });
  } else {
    localStorage.setItem('token', result.response.data.accessToken);
    localStorage.setItem('ref_token', result.response.data.refreshToken);
    dispatch(setAuth(true));
    dispatch(setStatus('data'));
  }
};

export const ShowUserAsync = (): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));

  const token = localStorage.getItem('token');
  const ref_token = localStorage.getItem('ref_token');

  const result = (await appServices.getUser(token!)) as any;
  if (isError(result)) {
    const result = (await appServices.refreshToken(ref_token!)) as any;
    if (isError(result)) {
      localStorage.removeItem('token');
      localStorage.removeItem('ref_token');
      dispatch(setAuth(false));
      dispatch(setStatus('error'));
    } else {
      localStorage.setItem('token', result.response.data.accessToken);
      localStorage.setItem('ref_token', result.response.data.refreshToken);
      dispatch(ShowUserAsync());
    }
  } else {
    dispatch(setUser(result.response.data));
    dispatch(setStatus('data'));
  }
};

export const selectUser = (state: RootState) => state.App;

export default AppSlice.reducer;
