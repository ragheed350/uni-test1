import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, RootState } from '../store';
import { Todo, Todo_D_Req } from '../../models/todo';
import requestStatus from '../../../constants/enums/request-status';

interface TodosState {
  status: requestStatus;
  todos: Todo[];
  todo?: Todo;
}

let initialState: TodosState = {
  status: 'no-thing',
  todos: [],
};

const TodosSlice = createSlice({
  name: 'Todos',
  initialState,
  reducers: {
    setStatus: (state, { payload }: PayloadAction<requestStatus>) => {
      state.status = payload;
    },
    InsertTodo: ({ todos }, { payload }: PayloadAction<Todo>) => {
      todos.push(payload);
    },
    ShowTodo: (state, { payload }: PayloadAction<number>) => {
      let el = state.todos.find((el: Todo) => el.id === payload);
      state.todo = el;
    },
    UpdateTodo: (state, { payload }: PayloadAction<Todo>) => {
      let ind = state.todos.findIndex((el) => el.id === payload.id);
      if (ind !== -1) state.todos[ind] = payload;
      console.log(payload);
    },
    DeleteTodo: ({ todos }, { payload }: PayloadAction<number>) => {
      let index = todos.findIndex((el) => el.id === payload);
      if (index !== -1) todos.splice(index, 1);
    },
    FetchTodos: (state) => {
      state.todos = JSON.parse(localStorage.getItem('todo-list')!);
    },
    UpdateLocalTodo: (state) => {
      localStorage.setItem('todo-list', JSON.stringify(state.todos));
    },
    FillTodos: (state, { payload }: PayloadAction<Todo[]>) => {
      localStorage.setItem('todo-list', JSON.stringify(payload));
    },
  },
});

const {
  setStatus,
  InsertTodo,
  UpdateTodo,
  DeleteTodo,
  FetchTodos,
  ShowTodo,
  FillTodos,
  UpdateLocalTodo,
} = TodosSlice.actions;

export const InsertTodoAsync = (req: Todo): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));
  dispatch(InsertTodo(req));
  dispatch(setStatus('data'));
  dispatch(UpdateLocalTodo());
};

export const ShowTodoAsync = (id: number): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));

  dispatch(ShowTodo(id));
  dispatch(setStatus('data'));

  // dispatch(UpdateLocalTodo());
};

export const UpdateTodoAsync = (req: Todo): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));
  dispatch(UpdateTodo(req));
  dispatch(setStatus('data'));

  dispatch(UpdateLocalTodo());
};

export const DeleteTodoAsync = (req: Todo_D_Req): AppThunk => async (
  dispatch
) => {
  dispatch(setStatus('loading'));
  dispatch(DeleteTodo(req.id));
  dispatch(setStatus('data'));

  dispatch(UpdateLocalTodo());
};

export const FetchTodosAsync = (): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));
  dispatch(FetchTodos());
  dispatch(setStatus('data'));

  // dispatch(UpdateLocalTodo());
};

export const FillTodosAsync = (req: Todo[]): AppThunk => async (dispatch) => {
  dispatch(setStatus('loading'));
  dispatch(FillTodos(req));
  dispatch(setStatus('data'));

  // dispatch(UpdateLocalTodo());
};

export const selectTodos = (state: RootState) => state.Todos.todos;
export const selectTodo = (state: RootState) => state.Todos.todo;
export const selectTodosStatus = (state: RootState) => state.Todos.status;

export default TodosSlice.reducer;
