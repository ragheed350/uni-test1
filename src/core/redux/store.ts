import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import todos from './todo';
import app from './app';

export const store = configureStore({
  reducer: { Todos: todos, App: app },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
