//..........
export interface Login_Req {
  email: string;
  password: string;
}

//...........
export interface Login_Res {
  accessToken: string;
  refreshToken: string;
}

//..........
export interface User {
  id: number;
  name: string;
  type: string;
  email: string;
}
