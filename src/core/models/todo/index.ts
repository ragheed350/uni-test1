//---------------Todo.ts---------------
export interface Todo {
  id: number;
  name: string;
  discreption: string;
}

//---------------Todo-I-Req.ts---------------

export interface Todo_I_Req {
  todo: Todo;
}

//---------------Todo-U-Req.ts---------------

export interface Todo_U_Req {
  id: number;
  todo: Todo;
}

//---------------Todo-D-Req.ts---------------
export interface Todo_D_Req {
  id: number;
}

//---------------Todo-S-Req.ts---------------
export interface Todo_S_Req {
  id: number;
}
