import React, { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LoginAsync, selectUser } from '../core/redux/app';
import { useHistory } from 'react-router';
import { Button, Col, Form, Input, Row } from 'antd';
import { Login_Req } from '../core/models/user';

interface props {}

const container_layout = {
  borderRadius: 25,
  display: 'grid',
  gridGap: '40px',
  boxShadow: '#c7c7c7 5px 8px 20px',
  padding: 40,
  margin: '60px auto',
};

export const styledInput: {
  style: React.CSSProperties;
} = {
  style: {
    borderRadius: 25,
    width: 400,
    outline: 'none',
    height: '50px',
    padding: '10px',
  },
};

const btn_layout = { minHeight: 40, minWidth: 100, margin: '30px 0px' };

const Login: FC<props> = (props) => {
  const dispatch = useDispatch();
  const { status } = useSelector(selectUser);
  const { replace } = useHistory();

  const onFinish = (values: Login_Req) => {
    dispatch(LoginAsync(values));
  };

  useEffect(() => {
    if (status === 'data') {
      replace('/');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  return (
    <Row>
      <Col style={container_layout}>
        <Row>
          <Col></Col>
        </Row>
        <Form onFinish={onFinish}>
          <Form.Item
            name='email'
            rules={[
              { required: true, message: 'email is required' },
              { type: 'email', message: 'email field should be a valid email' },
            ]}
          >
            <Input {...styledInput} placeholder='email' type='email' />
          </Form.Item>

          <Form.Item
            name='password'
            style={{ margin: '0px' }}
            rules={[
              { required: true, message: 'password field is required' },
              () => ({
                validator(_, value) {
                  if (value && value.length >= 8) {
                    return Promise.resolve();
                  }
                  return Promise.reject('password length should be 8 at least');
                },
              }),
            ]}
          >
            <Input.Password
              {...styledInput}
              placeholder={'password'}
              type='password'
            />
          </Form.Item>

          <Row>
            <Form.Item style={{ marginBottom: 0 }}>
              <Button
                style={btn_layout}
                shape='round'
                type='primary'
                htmlType='submit'
                loading={status === 'loading'}
              >
                Login
              </Button>
            </Form.Item>
          </Row>
        </Form>
      </Col>
    </Row>
  );
};
export default Login;
