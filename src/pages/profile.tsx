import { Col, Row, Space, Spin, Typography } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import React, { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { selectUser, ShowUserAsync } from '../core/redux/app';

const { Title, Text } = Typography;

const container_layout: React.CSSProperties = {
  borderRadius: 25,
  display: 'grid',
  gridGap: '40px',
  boxShadow: '#c7c7c7 5px 8px 20px',
  padding: 40,
  margin: '12% auto',
  textAlign: 'center',
  position: 'relative',
  minHeight: 360,
  minWidth: 400,
};

const avatar_style: React.CSSProperties = {
  position: 'absolute',
  top: '-25%',
};

interface props {}

const Profile: FC<props> = (props) => {
  const { user, status } = useSelector(selectUser);
  const dispatch = useDispatch();
  const { replace } = useHistory();

  useEffect(() => {
    dispatch(ShowUserAsync());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const token = localStorage.getItem('token');
  useEffect(() => {
    if (!token) {
      replace('/login');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  return (
    <Spin spinning={status === 'loading'}>
      <Row>
        <Col style={container_layout}>
          <Row align='bottom' justify='center'>
            <Col style={avatar_style} span={24}>
              <Avatar
                style={{
                  boxShadow: '#777777 3px 4px 10px',
                }}
                size={200}
                src='https://images.pexels.com/photos/2182970/pexels-photo-2182970.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
              />
            </Col>
            <Space direction='vertical' size='small'>
              <Col span={24}>
                <Title level={1}> {user?.name}</Title>
              </Col>
              <Col span={24}>
                <Text style={{ fontSize: '1.3em' }}> {user?.email}</Text>
              </Col>
              <Col span={24}>
                <Text style={{ fontSize: '1.3em' }}> {user?.type}</Text>
              </Col>
            </Space>
          </Row>
        </Col>
      </Row>
    </Spin>
  );
};
export default Profile;
