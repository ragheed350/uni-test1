import { Button, Col, Form, Input, Modal, Row } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import TodoCard from '../components/todo-card';
import {
  selectTodos,
  FetchTodosAsync,
  InsertTodoAsync,
} from '../core/redux/todo';
import { useIsMount } from '../utils/helpers/is-mount copy';

import './style.css';

interface props {}

const Home: FC<props> = (props) => {
  const todos = useSelector(selectTodos);
  const dispatch = useDispatch();
  const { replace } = useHistory();
  const token = localStorage.getItem('token');
  const [showModal, setshowModal] = useState(false);

  const isMount = useIsMount();

  useEffect(() => {
    if (!isMount) {
      localStorage.setItem(
        'todo-list',
        JSON.stringify([
          {
            name: 'Clean kitchen',
            discreption: "Don't forget the are under the sink!!",
          },
          {
            name: 'Call Eric',
            discreption: 'Remind him to do his taxes',
          },
          {
            name: 'Water flowers',
            discreption: "Don't forget the ones in the garden!",
          },
        ])
      );
    }

    dispatch(FetchTodosAsync());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleOk = () => {};

  const handleCancel = () => {
    setshowModal(false);
  };

  const onFinish_addTodo = (values: any) => {
    dispatch(InsertTodoAsync(values));
    setshowModal(false);
  };

  const add_modal = (
    <>
      <Modal
        title='add new todo'
        visible={showModal}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <Form name='todo' onFinish={onFinish_addTodo}>
          <Form.Item
            label='Name'
            name='name'
            rules={[{ required: true, message: 'Please input title!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label='Discreption'
            name='discreption'
            rules={[{ required: true, message: 'Please input discreption!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type='primary' htmlType='submit'>
              Add
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );

  return (
    <>
      <header>
        <div className='nav_bar'>
          <Button
            onClick={() => replace(token ? '/profile' : '/login')}
            size='large'
            type='primary'
            style={{ color: '#fff' }}
          >
            {token ? 'Show my profile' : 'Login'}
          </Button>
        </div>
      </header>
      {/* todo list */}
      <Row justify='center'>
        <Col span={16}>
          <Row justify='center' gutter={[32, 24]} style={{ marginTop: '10%' }}>
            <Button
              onClick={() => setshowModal(true)}
              size='large'
              type='primary'
              style={{ color: '#fff' }}
            >
              Add new todo
            </Button>
            {todos.map((todo) => (
              <TodoCard key={todo.id} todo={todo} />
            ))}
          </Row>
        </Col>
      </Row>
      {add_modal}
    </>
  );
};
export default Home;
