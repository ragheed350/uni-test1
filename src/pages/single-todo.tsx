import React, { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import {
  DeleteTodoAsync,
  selectTodo,
  selectTodosStatus,
  ShowTodoAsync,
  UpdateTodoAsync,
} from '../core/redux/todo';

interface props {}

const SingleTodo: FC<props> = (props) => {
  const todo = useSelector(selectTodo);
  const status = useSelector(selectTodosStatus);
  const dispatch = useDispatch();
  const { id }: any = useParams();
  const { replace } = useHistory();

  useEffect(() => {
    id && dispatch(ShowTodoAsync(Number(id)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  const delete_todo = () => {
    todo && dispatch(DeleteTodoAsync({ id: todo.id }));
    replace('/');
  };

  const [open, setopen] = useState(false);
  const [up_todo, setup_todo] = useState<any>({
    id: todo?.id,
    name: todo?.name,
    discreption: todo?.discreption,
  });

  useEffect(() => {
    if (todo) {
      setup_todo({
        id: todo?.id,
        name: todo?.name,
        discreption: todo?.discreption,
      });
    }
  }, [todo]);

  const handleClose = () => {
    setopen(false);
  };

  const handleOk = () => {
    up_todo && dispatch(UpdateTodoAsync(up_todo));
    setopen(false);
  };

  return <></>;
};
export default SingleTodo;
