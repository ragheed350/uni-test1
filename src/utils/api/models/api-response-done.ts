export default interface ApiResponseDone<T = any> {
  response: T;
  data: T;
  status: boolean;
  message: string;
  code: number;
}
